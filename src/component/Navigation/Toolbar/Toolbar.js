import React, { Component } from 'react';

import classes from './Toolbar.css';
import NavItems from '../NavItems/NavItems';

class Toolbar extends Component {
    render(){
    const <header className={classes.Toolbar}>
        <nav className={classes.DesktopOnly}>
            <NavItems />
        </nav>
    </header>
    }
}

export default Toolbar;