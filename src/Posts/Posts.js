import React,{Component} from 'react';
import axios from '../axios';
import './Posts.css';
import Post from './Post';

class Posts extends Component{
    state={
        num:4,
        posts:[]
    }
    componentDidMount(){
        let inc =this.state.num;
        axios.get('/posts')
        .then(response=>{
            const posts=response.data.slice(0,inc);
            const updatedPosts=posts.map(post=>{
                return{
                    ...post,
                    author:'Yash'
                }
            });
            this.setState({posts:updatedPosts});
            // console.log(response);
        })
        .catch(error=>{
            console.log(error);
            //this.setState({error:true});
            
        })
        ;
    }
    addi=()=>{
        
        this.setState({num:this.state.num+1}) ;
        let inc =this.state.num;
        axios.get('/posts')
        .then(response=>{
            const posts=response.data.slice(0,inc);
            const updatedPosts=posts.map(post=>{
                return{
                    ...post,
                    author:'Yash'
                }
            });
            this.setState({posts:updatedPosts});
            // console.log(response);
        })
        .catch(error=>{
            console.log(error);
            //this.setState({error:true});
            
        })
        ;
    }
    deleteHandler= () => {
        axios.delete('/posts/'+this.props.id)
        .then(response=>{
            console.log(response);
        });
        
    }
    render(){
        let posts=<p style={{textAlign:"center"}}>Something went wrong</p>
        if (!this.state.error){
            posts=this.state.posts.map(post=>{
                return (
                    //<Link to={'/'+post.id}  >
                        <Post 
                        key={post.id}
                        author={post.author}
                        title={post.title}
                        removed={this.deleteHandler()}
                        clicked={()=>this.postSelectedHandler(post.id)}/>
                    //</Link>
                    );
            });
        } 
        return(
            
            <div>
                <div>
                    <h2>Add Post</h2>
                 <button className="btn-add" onClick={this.addi}>Add</button>
            </div>
            <br>
            </br>
            <br
></br>                <section className="Posts">
                        {posts}

                </section>
            </div>
        );
    }
}

export default Posts;