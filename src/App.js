import React, { Component } from 'react';
import './App.css';
import NavBar from './components/NavBar/NavBar';
import Home from './components/Home/Home';
//import Home from './components/Home/Home';
import {Route,Switch} from 'react-router-dom';
import User from './Taskbar/User';
import Posts from './Posts/Posts';
import Login from './login/Login2';

class App extends Component {
  render() {
    return (
      <div className="App">
        <NavBar />
        <br>
        </br>
        <br></br><br>
        </br>
        <br></br><br>
        </br>
        <br></br>
        
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/task" component={Posts} />
          <Route path="/user" component={User} />
          </Switch>
        
      </div>
    );
  }
}

export default App;
