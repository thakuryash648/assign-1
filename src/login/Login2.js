import React,{Component} from 'react';
import Input from './Input/Input';
import Button from '../UI/Button/Button';
import classes from './Login.css';

export default class Login extends Component{
    constructor(props){
        super(props);
        this.state={
            username:'',
            password:'',
            errorMessase:'',
            successMessage:''
        }
        this.reset=this.reset.bind(this);
    }
    setValue(e){
        let id=e.target.id;
        let value=e.target.value;
        let username=this.state.username;
        if(value===undefined||value===''){
            return; 
        }
        if(id==='username'){
            username=value;
            this.setState({
                username:username
            })
        }
        else{
            this.setState({
                    password:e.target.value
                })
        }
    }
    submit(e){
        if(this.state.username===undefined||this.state.username===''
        ||this.state.password===undefined || this.state.password === ''){
            this.setState({
                errorMessase:'Enter Both the fields'
            })
        }
        if(this.state.username==='test@test.com' && this.state.password==='12345678'){
            this.setState({
                successMessage:'Welcome admin'
            })
        }
        else{
            this.setState({
                successMessage:'Please provide correct details'
            })
        }
    }
    reset(){
        let username=this.state.username;
        let password=this.state.password;
        this.setState({
            username:'',
            password:'',
        })
    }
    render(){
        return(
            <div className="login-form">
                <h2>Login Form</h2>
              <b>Username:</b>
                <input type="text" id="username" value={this.state.username} onChange={(e)=>this.setValue(e)} />
                <b>Password:</b><input type="password" id="password" value={this.state.password} onChange={(e)=>this.setValue(e)} />
                <p>{this.state.errorMessase}</p>
                <p>{this.state.successMessage}</p>
                <button className="btn-design" onClick={(e)=>this.submit(e)}>login</button>
                <button  className="btn-design" onClick={this.reset}>logout</button>
            </div>
        )
    }
}


