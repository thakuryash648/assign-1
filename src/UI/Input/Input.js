import React from 'react';
import classes from './Input.css';

const input = (props) =>{
    
    let  inputElement=(
                <select 
                    value={props.value} 
                    onChange={props.changed}  >
                    {props.elementConfig.options.map(option => (
                        <option key={option.value} value={option.value}>
                            {option.displayValue}
                        </option>
                    ))}
                </select>
                );
    return(
        <div className={classes.Input}>
            <label className={classes.Label}>{props.label}</label>
            {inputElement}
        </div>
    );
    
};

export default input;