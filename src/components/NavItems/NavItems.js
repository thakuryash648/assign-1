import React from 'react';
import NavItem from './NavItem/NavItem';
import './NavItems.css';



const NavItems = () => (
    <ul className="NavItems">
        <NavItem link="/">Home</NavItem>
        <NavItem link="/task">Tasks</NavItem>
        <NavItem link="/user">user</NavItem>
    </ul>
);

export default NavItems;