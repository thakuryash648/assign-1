import React,{Component} from 'react';

class Home extends Component{
    state={
        data:'hii'
    }

    chane=(event)=>{
        this.setState({dat:event.target.value});
        switch (event.target.value){
            case "Animals":
                this.setState({data:'Types of animals'});
                break;
            case "Birds":
                this.setState({data:'Types of Birds'});
                break;
            case "Humans":
                this.setState({data:'Types of Humans'});
                break;
            default:
                this.setState({data:'please select'});
               break;
        }
    }
    render(){
        let form=(
            <form>
            <div className="form-group">
            <select onChange={this.chane} className="form-control">
                <option selected hidden> select</option>

                <option value="Animals">Animals</option>
                <option value="Birds">Birds</option>
                
                <option value="Humans">Humans</option>
            </select>
            </div>
            <br></br>
            <br></br>

                <div>{this.state.data}</div>
            </form>
        );
        
        return(
            <div>
                <h2>Select about the data to be viewed</h2>
                <br></br>
                <br></br>
                {form}
                <p></p>
            
            </div>
        );
    }
};

export default Home;