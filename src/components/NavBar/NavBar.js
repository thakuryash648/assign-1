import React from 'react';
import './NavBar.css';
import NavItems from '../NavItems/NavItems';

const NavBar = (props) => (
    <div className="navbar-design">
    <header className="NavBar">
        <div>MENU</div>
        <nav className="NavBarItem">
            <NavItems />
            
        </nav>
    </header> 
    </div>
);

export default NavBar;